package Prova01;

public interface PilhaInterface {
    void inicializar(int qtdElementos);
    void empilhar(Object dado);
    Object desempilhar();
    boolean estaVazia();
    boolean estaCheia();
}
