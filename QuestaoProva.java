package Prova01;

public abstract class QuestaoProva {

    public abstract void adcionar(int valor);

    public abstract int obterProximo(Tipo tipo);

    public abstract void excluirTodos(Tipo tipo);

    int caṕacidadeMaxPorTipo = 0;

    public QuestaoProva(int capacidadeMaxPorTipo) {
        this.caṕacidadeMaxPorTipo = capacidadeMaxPorTipo;
    }

}


