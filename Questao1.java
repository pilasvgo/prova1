package Prova01;

public class Questao1 {

    private static int tamanhoA = 3;
    private static int tamanhoB = 3;
    private static boolean validador = true;


    public static void main(String[] args) {

        Questao1 prova01 = new Questao1();

        Pilha pilhaA = new Pilha();
        pilhaA.inicializar(prova01.tamanhoA);

        Pilha pilhaB = new Pilha();
        pilhaB.inicializar(prova01.tamanhoB);

        pilhaA.empilhar(1);
        pilhaA.empilhar(2);
        pilhaA.empilhar(3);

        pilhaB.empilhar(1);
        pilhaB.empilhar(2);
        pilhaB.empilhar(3);

        if (iguais(pilhaA, pilhaB)) {
            System.out.println("Igual");
        } else {
            System.out.println("Diferente");
        }

    }

/*
    public static boolean iguais(IPilha pilhaA, IPilha pilhaB) {
        boolean validador = true;
        if(tamanhoA!=tamanhoB){
            validador = false;
        }else {
            Object auxA, auxB;
            for (int i =0; i< tamanhoA; i++){
                auxA = pilhaA.desempilhar();
                auxB = pilhaB.desempilhar();
                if(!auxA.equals(auxB)){
                    validador = false;
                    break;
                }
            }
        }
        return validador;
    }
*/

    public static boolean iguais(IPilha pilhaA, IPilha pilhaB) {
        if (tamanhoA != tamanhoB) {
            validador = false;
        } else {
            Object auxA, auxB;
            auxA = pilhaA.desempilhar();
            auxB = pilhaB.desempilhar();
            if (!auxA.equals(auxB)) {
                validador = false;
            }
            if (!pilhaA.estaVazia()) {
                iguais(pilhaA, pilhaB);
            }
            pilhaA.empilhar(auxA);
            pilhaB.empilhar(auxB);
        }
        return validador;
    }

}
