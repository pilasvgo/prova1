package Prova01;

public interface IPilha {
    void empilhar(Object o);
    Object desempilhar();
    boolean estaVazia();

}
