package Prova01;

public interface FilaInterface {

    public void Queue(int valor);
    public int DeQueue();
    public boolean empty();
    public int Size();
}