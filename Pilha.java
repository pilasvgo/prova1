package Prova01;


import java.util.Arrays;

public class Pilha implements PilhaInterface,IPilha {

    private Object[] list;
    private int posicao;
    private int pesquisaPosicao = -1;
    private String nome;

    public Pilha() {
    }

    public Pilha(String nome) {
        this.nome = nome;
    }

    public void inicializar(int qtdElementos) {
        try {
            if (list.length > 0) {
                System.out.println("Pilha já iniciada!");
            } else {
                this.list = new Object[qtdElementos];
                posicao = 0;
            }
        } catch (Exception e) {
            this.list = new Object[qtdElementos];
            posicao = 0;
        }


    }

    public void empilhar(Object dado) {
        if (!estaCheia()) {
            this.list[posicao] = dado;
            this.posicao++;
        } else {
            System.out.println("Pilha está cheia!");
        }
    }

    public Object desempilhar() {
        Object aux;
        if (!estaVazia()) {
            this.posicao--;
            aux = list[posicao];
            return aux;
        } else {
            return null;
        }

    }

    public boolean estaVazia() {
        if (this.posicao == 0) {
            return true;
        } else {
            return false;
        }

    }

    public boolean estaCheia() {
        if (posicao < this.list.length) {
            return false;
        } else {
            return true;
        }
    }



    public Object[] getList() {
        return list;
    }

    public void setPesquisaPosicao(int pesquisaPosicao) {
        this.pesquisaPosicao = pesquisaPosicao;
    }

    public String getNome() {
        return nome;
    }

    @Override
    public String toString() {
        return "Pilha{" +
                "list=" + Arrays.toString(list) +
                '}';
    }
}
