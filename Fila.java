package Prova01;

import java.util.Arrays;

public class Fila implements FilaInterface {


    private int[] valor;
    private int inicio = 0;
    private int fim = -1;
    private int max;

    public Fila(int max) {
        this.max = max;
        valor = new int[max];
    }

    public void Queue(int valor) {
        if(Size() < max){
            this.fim++;
            this.valor[this.fim] = valor;
        }else{
            throw new FilaCheiaException("Fila cheia!");
        }
    }

    public int DeQueue() {
        if(!empty()){
            int intAux = this.valor[this.inicio];
            for (int i = 1; i <= this.fim; i++){
                this.valor[i-1] = valor[i];
            }
            this.fim--;
            return intAux;
        }else {
            throw new FilaVaziaException("Fila vazia!");
        }

    }

    public boolean empty() {
        if(this.fim < 0){
            return true;
        }else{
            return false;
        }
    }

    public int Size() {
        return fim+1;
    }

    public int[] getvalor() {
        return valor;
    }

    public void setvalor(int[] pilhas) {
        valor = pilhas;
    }

    public int getInicio() {
        return inicio;
    }

    public void setInicio(int inicio) {
        this.inicio = inicio;
    }

    @Override
    public String toString() {
        return "Fila{" +
                "valor=" + Arrays.toString(valor) +
                ", inicio=" + inicio +
                ", fim=" +
                '}';
    }
}

class FilaCheiaException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public FilaCheiaException() {
        super();
    }

    public FilaCheiaException(String message) {
        super(message);
    }

}

class FilaVaziaException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public FilaVaziaException() {
        super();
    }

    public FilaVaziaException(String message) {
        super(message);
    }

}

