package Prova01;

public class Questao3 extends QuestaoProva {


    Fila filas[] = new Fila[4];
    Tipo tipo;
    Fila f1, f2, f3, f4;

    public static void main(String[] args) {

        Questao3 questao3 = new Questao3(4);

    }

    public Questao3(int capacidadeMaxPorTipo) {
        super(capacidadeMaxPorTipo);
        this.f1 = new Fila(capacidadeMaxPorTipo);
        this.f2 = new Fila(capacidadeMaxPorTipo);
        this.f3 = new Fila(capacidadeMaxPorTipo);
        this.f4 = new Fila(capacidadeMaxPorTipo);

    }

    public void adcionar(int valor) {
        try {
            if (valor % 2 == 0 && valor > 0) {
                this.tipo = Tipo.MaiorQZeroPar;
                f1.Queue(valor);
            }
            if (valor % 2 != 0 && valor > 0) {
                this.tipo = Tipo.MaiorQZeroImpar;
                f2.Queue(valor);
            }
            if (valor % 2 == 0 && valor <= 0) {
                this.tipo = Tipo.MenorIgualZeroPar;
                f3.Queue(valor);
            }
            if (valor % 2 != 0 && valor <= 0) {
                this.tipo = Tipo.MenorIgualZeroImpar;
                f4.Queue(valor);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }

    public int obterProximo(Tipo tipo) {
        int aux = 0;
        try {

            if (tipo == Tipo.MaiorQZeroPar) {
                aux = f1.DeQueue();
            }
            if (tipo == Tipo.MaiorQZeroImpar) {
                aux = f2.DeQueue();
            }
            if (tipo == Tipo.MenorIgualZeroPar) {
                aux = f3.DeQueue();
            }
            if (tipo == Tipo.MenorIgualZeroImpar) {
                aux = f4.DeQueue();
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return aux;
    }

    public void excluirTodos(Tipo tipo) {
        try {
            if (tipo == Tipo.MaiorQZeroPar && !f1.empty()) {
                this.f1.DeQueue();
                excluirTodos(tipo);
            }
            if (tipo == Tipo.MaiorQZeroImpar && !f2.empty()) {
                this.f2.DeQueue();
                excluirTodos(tipo);
            }
            if (tipo == Tipo.MenorIgualZeroPar && !f3.empty()) {
                this.f3.DeQueue();
                excluirTodos(tipo);
            }
            if (tipo == Tipo.MenorIgualZeroImpar && !f4.empty()) {
                this.f4.DeQueue();
                excluirTodos(tipo);
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }
    
}
